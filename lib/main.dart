import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(ProfileApp());

class ProfileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            backgroundColor: Colors.blueGrey[600], body: ProfilePage()));
  }
}

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.blueGrey[900], Colors.blueGrey[500]])),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(blurRadius: 3, color: Colors.black),
                  ]),
              child: CircleAvatar(
                radius: 50,
                backgroundImage:
                    AssetImage('assets/images/profile_picture.png'),
              ),
            ),
            Text(
              'Denis López Fernández',
              style: TextStyle(
                fontFamily: 'Pacifico',
                fontSize: 24,
                color: Colors.white,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image(
                  height: 20,
                  image: AssetImage('assets/images/flutter_logo.png'),
                  color: null,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Flutter Developer',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 18,
                      color: Colors.blueGrey[100],
                      letterSpacing: 2),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
              child: Divider(
                color: Colors.blueGrey[50],
              ),
            ),
            ContactEntry(
                'denis.lfernandez@hotmail.com',
                'mailto:denis.lfernandez@hotmail.com',
                'assets/images/outlook_logo.png'),
            ContactEntry('@denislfernandez', 'https://t.me/denislfernandez',
                'assets/images/telegram_logo.png'),
            ContactEntry(
                'https://www.linkedin.com/in/denislfernandez/',
                'https://www.linkedin.com/in/denislfernandez/',
                'assets/images/linkedin_logo.png'),
            ContactEntry(
                'https://gitlab.com/denislfernandez',
                'https://gitlab.com/denislfernandez',
                'assets/images/gitlab_logo.png'),
          ],
        ),
      ),
    );
  }
}

class ContactEntry extends StatelessWidget {
  final String text;
  final String url;
  final String imageAsset;

  ContactEntry(this.text, this.url, this.imageAsset);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: ListTile(
        onTap: () => _launchURL(url),
        leading: Image(
          height: 24,
          image: AssetImage(imageAsset),
        ),
        title: Text(
          text,
          style: TextStyle(fontSize: 14),
        ),
      ),
    );
  }
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
